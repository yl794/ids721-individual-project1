# IDS-721 Individual project1 yl794

## static webpage
After running 'zola serve', we can vist the site at localhost:1111
![](1.png)
After clicking 'Portfolio' button or enter from the head bar, we can visit the portfolio page.
![](2.png)
After entering the project, we can see the readme file.
![](3.png)

## cicd
I setup the gitlab workflow by configuing `.gitlab-ci.yml` file.
![](4.png)

## deploy
The weboage is hosted by gitlab pages
![](5.png)
https://individual-project1-yl794-fcb3dad4d1f2a4bd70120c645c0d02d8758f5.pages.oit.duke.edu/

## video
demo video: https://youtu.be/IX-D4rWbSok
