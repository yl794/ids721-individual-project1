+++
title = "Individual Project"
description = "Use Zola to develop a static site."
date = 2024-03-10T09:19:42+00:00
updated = 2024-03-10T09:19:42+00:00
draft = false
template = "portfolio/page.html"

[extra]
lead = "This is the readme of the <b>individual</b> program."
+++

In this project, I hosted the zola webpage on gitlab pages.
Please visit the site at https://ids721-individual-project1-yl794-94dd777d404cddd32ef48aad1c5d1e.pages.oit.duke.edu/
